<script src="<?= base_url()?>assets/js/jquery.min.js"></script>

<!-- INPUT FORM -->

<form id="_form">
<input type="hidden" name="id" id="id" class="input-id">
<input type="hidden" name="action" id="action" value="add" class="input-id">
<input type="text" name="name" required id="name" class="input-name" placeholder="Enter your name">
<br>
<button type="submit">SUBMIT</button>
</form>


<!-- POPULATE DATA -->
<table>
    <thead>
        <th>
            #
        </th>
        <th>
            Name
        </th>
        <th>
            Action
        </th>
    </thead>

    <tr class="table-list-template" style="display:none;">
        <th class="table-list-count">
        </th>
        <td class="table-list-name">
        </td>
        <td>
            <button class="table-update">update</button>
            <button class="table-delete">delete</button>
        </td>
    </tr>

    <tbody class="table-list">
    </tbody>
</table>

<script>
    var base_url = "<?= base_url();?>";

    $(document).ready(function(){

    var uiListContainer = $(".table-list"),
        uiListTemplate  = $(".table-list-template");

        function clear_form(){
            $("#id").val("");
            $("#action").val("add");
            $("#name").val("");
        }

        function load_table(){

            $.ajax({
                url : base_url + "index.php/review/load_table",
                type: "POST",
                data: "",
                datatype: "JSON",
                returntype: "JSON",
                beforeSend: function(){
                    uiListContainer.empty();
                },
                success: function(oData){
                    var data = oData.data;

                    if(data.length > 0){
                        for (var i in data){
                            
                            var uiListClone = uiListTemplate.clone();
                            uiListClone.addClass('table-list-cloned');
                            uiListClone.removeClass('table-list-template');
                            uiListClone.find('.table-list-count').html(parseInt(i) + parseInt(1));
                            uiListClone.find('.table-list-name').html(data[i].name);
                            uiListClone.data(data[i]);
                            uiListClone.show();

                            uiListContainer.append(uiListClone);  
                        }

                        var btnUpdate = $('.table-list-cloned').find('.table-update');
                        var btnDelete = $('.table-list-cloned').find('.table-delete');

                        btnUpdate.off('click').on('click', function(){
                            oParentData = $(this).closest('.table-list-cloned');
                            $("#id").val(oParentData.data('id'));
                            $("#action").val("update");
                            $("#name").val(oParentData.data('name'));
                        });

                        btnDelete.off('click').on('click', function(){
                            oParentData = $(this).closest('.table-list-cloned');

                            var confirm = window.confirm("Are you sure you want to delete, " + oParentData.data('name') + "?");
                            if(confirm){
                                // alert("Confirmed");
                                $.ajax({
                                    url: base_url + "index.php/review/save",
                                    type: "POST",
                                    data: {
                                        id: oParentData.data('id'),
                                        action: "delete"
                                    },
                                    datatype: "JSON",
                                    success: function(data){
                                        clear_form();
                                        load_table();
                                        alert("Successfully Deleted!");
                                    }
                                })
                            }
                        });
                    }else{
                        uiListContainer.append("No Result Found");
                    }
                }
            })
        }

        load_table();

        $("#_form").submit(function(e){
            e.preventDefault();

            $.ajax({
                url : base_url + "index.php/review/save",
                type: "POST",
                data: $(this).serializeArray(),
                datatype: "JSON",
                success: function(data){
                    clear_form();
                    load_table();
                    alert("Successfully Save!");
                }
            });
        });
    });
</script>