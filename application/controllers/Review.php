<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("Crud_model");
    }

	public function index()
	{
		$this->load->view('crud');
    }
    
    function save(){

        switch ($this->input->post('action')) {
            case 'add':
                    $result['data'] = $this->Crud_model->add($this->input->post());
                break;

            case 'update':
                    $result['data'] = $this->Crud_model->update($this->input->post());
                break;
                
            case 'delete':
                    $result['data'] = $this->Crud_model->delete($this->input->post());
                break;
            
            default:
                break;
        }

        header("Content-Type: applicaton/json");
        echo json_encode($result);
    }
    
    function load_table(){
        $result['data'] = $this->Crud_model->load_table();

        header("Content-Type: applicaton/json");
        echo json_encode($result);
    }
}
