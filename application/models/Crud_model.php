<?php

class Crud_model extends CI_MODEL{

	function add($data){
		$q = "INSERT INTO sample_table(
			name) VALUES('".$data['name']."')";

        $this->db->query($q);
        return $this->db->insert_id();
	}

	function update($data){
		$q = "UPDATE sample_table
			SET name = '" . $data['name'] . "'
			WHERE id = " . $data['id'];

		$result = $this->db->query($q);
		return $this->db->affected_rows();
	}

	function delete($data){
		$q = "UPDATE sample_table
			SET is_deleted = 1
			WHERE id = " . $data['id'];

		$result = $this->db->query($q);
		return $this->db->affected_rows();
	}

    function load_table(){
        $q = "SELECT * FROM sample_table where is_deleted = 0";

        $result = $this->db->query($q);
        return $result->result_array();
    }
}

?>